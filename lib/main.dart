import 'package:flutter/material.dart';

import 'variables.dart';
import 'varnsdorf_solving.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'AppVesto Secondary Intern Test App',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: MyHomePage(title: 'Knights tour solver'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _colorTile = false;

  void switchColor() {
    _colorTile ? _colorTile = false : _colorTile = true;
  }

  void zeroVars() {
    x = 0;
    y = 0;
    col = 0;
  }

  @override
  void initState() {
    super.initState();
    fillWithZeros();
    sliceA();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 10.0),
                child: Text(
                  'Enter starting coordinates. Numeration starts from 1.',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                ),
              ),
              Row(
                children: <Widget>[
                  Flexible(
                    child: TextField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Enter x position.(9 > x > 0)',
                        contentPadding: const EdgeInsets.only(left: 20.0, top: 15.0, right: 10.0),
                      ),
                      onChanged: (text) {
                        x = int.parse(text);
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10.0, right: 12.0),
                    child: RaisedButton(
                      onPressed: () {
                        solveChess();
                        aToB();
                        setState(() {});
                      },
                      child: Text('SOLVE'),
                    ),
                  ),
                ],
              ),
              TextField(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Enter y start position.(9 > y > 0)',
                  contentPadding: EdgeInsets.only(left: 20.0, top: 15.0, right: 10.0),
                ),
                onChanged: (text) {
                  y = int.parse(text);
                },
              ),

              GridView.builder(
                  shrinkWrap: true,
                  itemCount: a.length * a[0].length,
                  physics: NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.all(10),
                  gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: a.length),
                  itemBuilder: (BuildContext context, int index) {
                    if (index%8 == 0) {
                      switchColor();
                    }
                    if (_colorTile) {
                      switchColor();
                      return Container(
                        margin: const EdgeInsets.all(2.0),
                        color: Colors.orangeAccent[400],
                        child: Center(
                          child: Text(
                            b[index].toString(),
                            style: txtStyle,
                          ),
                        ),
                      );
                    } else {
                      switchColor();
                      return Container(
                        margin: const EdgeInsets.all(2.0),
                        color: Colors.brown[600],
                        child: Center(
                          child: Text(
                            b[index].toString(),
                            style: txtStyle,
                          ),
                        ),
                      );
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }
}

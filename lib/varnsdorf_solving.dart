import 'variables.dart';

void fillWithZeros() {
  for (i = 0; i < 8; i++) {
    for (j = 0; j < 8; j++) {
      a[i][j] = 0;
    }
  }
}

void sliceA() {
  for (i = 0; i < 8; i++) {
    for (j = 0; j < 8; j++) {
      b.add(a[i][j]);
    }
  }
}

void aToB() {
  int bCount = 0;
  for (i = 0; i < 8; i++) {
    for (j = 0; j < 8; j++) {
      b[bCount] = (a[i][j]);
      bCount++;
    }
  }
}

void solveChess() {
  if ((x <= 0 || y <= 0) || (x > 8 || y > 8)) {
    fillWithZeros();
  } else {
    x--;
    y--;

    rangeCheck(x, y, a) {
      int t = 0;
      if (x > 0 && y > 1 && a[x - 1][y - 2] == 0) {
        t++;
      }
      if (x > 0 && y < 6 && a[x - 1][y + 2] == 0) {
        t++;
      }
      if (x > 1 && y > 0 && a[x - 2][y - 1] == 0) {
        t++;
      }
      if (x > 1 && y < 7 && a[x - 2][y + 1] == 0) {
        t++;
      }
      if (x < 7 && y > 1 && a[x + 1][y - 2] == 0) {
        t++;
      }
      if (x < 7 && y < 6 && a[x + 1][y + 2] == 0) {
        t++;
      }
      if (x < 6 && y > 0 && a[x + 2][y - 1] == 0) {
        t++;
      }
      if (x < 6 && y < 7 && a[x + 2][y + 1] == 0) {
        t++;
      }
      return t;
    }

    while (col < 64) {
      col++;
      a[x][y] = col;
      tmp = 9;

      if (x > 0 && y > 1 && a[x - 1][y - 2] == 0) {
        int tmp1 = rangeCheck(x - 1, y - 2, a);
        if (tmp1 < tmp) {
          i = x - 1;
          j = y - 2;
          tmp = tmp1;
        }
      }
      if (x > 0 && y < 6 && a[x - 1][y + 2] == 0) {
        int tmp1 = rangeCheck(x - 1, y + 2, a);
        if (tmp1 < tmp) {
          i = x - 1;
          j = y + 2;
          tmp = tmp1;
        }
      }
      if (x > 1 && y > 0 && a[x - 2][y - 1] == 0) {
        tmp1 = rangeCheck(x - 2, y - 1, a);
        if (tmp1 < tmp) {
          i = x - 2;
          j = y - 1;
          tmp = tmp1;
        }
      }
      if (x > 1 && y < 7 && a[x - 2][y + 1] == 0) {
        tmp1 = rangeCheck(x - 2, y + 1, a);
        if (tmp1 < tmp) {
          i = x - 2;
          j = y + 1;
          tmp = tmp1;
        }
      }
      if (x < 7 && y > 1 && a[x + 1][y - 2] == 0) {
        tmp1 = rangeCheck(x + 1, y - 2, a);
        if (tmp1 < tmp) {
          i = x + 1;
          j = y - 2;
          tmp = tmp1;
        }
      }
      if (x < 7 && y < 6 && a[x + 1][y + 2] == 0) {
        tmp1 = rangeCheck(x + 1, y + 2, a);
        if (tmp1 < tmp) {
          i = x + 1;
          j = y + 2;
          tmp = tmp1;
        }
      }
      if (x < 6 && y > 0 && a[x + 2][y - 1] == 0) {
        tmp1 = rangeCheck(x + 2, y - 1, a);
        if (tmp1 < tmp) {
          i = x + 2;
          j = y - 1;
          tmp = tmp1;
        }
      }
      if (x < 6 && y < 7 && a[x + 2][y + 1] == 0) {
        tmp1 = rangeCheck(x + 2, y + 1, a);
        if (tmp1 < tmp) {
          i = x + 2;
          j = y + 1;
          tmp = tmp1;
        }
      }
      x = i;
      y = j;
    }
  }
}
